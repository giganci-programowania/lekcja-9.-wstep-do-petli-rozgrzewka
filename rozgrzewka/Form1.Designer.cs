﻿namespace rozgrzewka
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.txtPierwsza = new System.Windows.Forms.TextBox();
            this.txtDruga = new System.Windows.Forms.TextBox();
            this.txtWynik = new System.Windows.Forms.TextBox();
            this.cbDzialanie = new System.Windows.Forms.ComboBox();
            this.btnWynik = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(38, 26);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(79, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Pierwsza liczba";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(65, 52);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(52, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "Działanie";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(51, 78);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(66, 13);
            this.label3.TabIndex = 2;
            this.label3.Text = "Druga liczba";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(80, 134);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(37, 13);
            this.label4.TabIndex = 3;
            this.label4.Text = "Wynik";
            // 
            // txtPierwsza
            // 
            this.txtPierwsza.Location = new System.Drawing.Point(123, 23);
            this.txtPierwsza.Name = "txtPierwsza";
            this.txtPierwsza.Size = new System.Drawing.Size(121, 20);
            this.txtPierwsza.TabIndex = 4;
            // 
            // txtDruga
            // 
            this.txtDruga.Location = new System.Drawing.Point(123, 75);
            this.txtDruga.Name = "txtDruga";
            this.txtDruga.Size = new System.Drawing.Size(121, 20);
            this.txtDruga.TabIndex = 5;
            // 
            // txtWynik
            // 
            this.txtWynik.Location = new System.Drawing.Point(123, 131);
            this.txtWynik.Name = "txtWynik";
            this.txtWynik.Size = new System.Drawing.Size(121, 20);
            this.txtWynik.TabIndex = 6;
            // 
            // cbDzialanie
            // 
            this.cbDzialanie.FormattingEnabled = true;
            this.cbDzialanie.Items.AddRange(new object[] {
            "Dodawanie",
            "Odejmowanie",
            "Mnożenie",
            "Dzielenie"});
            this.cbDzialanie.Location = new System.Drawing.Point(123, 49);
            this.cbDzialanie.Name = "cbDzialanie";
            this.cbDzialanie.Size = new System.Drawing.Size(121, 21);
            this.cbDzialanie.TabIndex = 7;
            // 
            // btnWynik
            // 
            this.btnWynik.Location = new System.Drawing.Point(146, 102);
            this.btnWynik.Name = "btnWynik";
            this.btnWynik.Size = new System.Drawing.Size(75, 23);
            this.btnWynik.TabIndex = 8;
            this.btnWynik.Text = "Oblicz wynik";
            this.btnWynik.UseVisualStyleBackColor = true;
            this.btnWynik.Click += new System.EventHandler(this.btnWynik_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(317, 175);
            this.Controls.Add(this.btnWynik);
            this.Controls.Add(this.cbDzialanie);
            this.Controls.Add(this.txtWynik);
            this.Controls.Add(this.txtDruga);
            this.Controls.Add(this.txtPierwsza);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtPierwsza;
        private System.Windows.Forms.TextBox txtDruga;
        private System.Windows.Forms.TextBox txtWynik;
        private System.Windows.Forms.ComboBox cbDzialanie;
        private System.Windows.Forms.Button btnWynik;
    }
}

