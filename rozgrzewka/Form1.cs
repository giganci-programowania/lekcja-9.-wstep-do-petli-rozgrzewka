﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace rozgrzewka
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        // Napisać prosty kalkulator z 4 operacjami: dodawanie, odejmowanie, mnożenie, dzielenie w WinForms.
        // Zabezpieczyć przed niepoprawnymi działaniami podawanymi przez użytkownika:
        //   a) dzielenie przez zero,
        //   b) wpisanie czegokolwiek innego niż liczba (instrukcja try..catch..).


        private void btnWynik_Click(object sender, EventArgs e)
        {
            try
            {
                string dzialanie = cbDzialanie.Text; // wybrany tekst
                float pierwsza = float.Parse(txtPierwsza.Text);
                float druga = float.Parse(txtDruga.Text);
                float wynik;
                switch (dzialanie)
                {
                    case "Dodawanie":
                        wynik = pierwsza + druga;
                        txtWynik.Text = wynik.ToString();
                        break;
                    case "Odejmowanie":
                        wynik = pierwsza - druga;
                        txtWynik.Text = wynik.ToString();
                        break;
                    case "Mnożenie":
                        wynik = pierwsza * druga;
                        txtWynik.Text = wynik.ToString();
                        break;
                    case "Dzielenie":
                        if (druga == 0)
                        {
                            throw new DivideByZeroException("Dzielenie przez zero jest nie dozwolone"); // rzucenie wyjątkiem w celu zabezpieczeniem się przed dzieleniem przez 0
                        }
                        wynik = pierwsza / druga;
                        txtWynik.Text = wynik.ToString();
                        break;
                }
            }
            catch(Exception ex)
            {
                if (ex is DivideByZeroException)
                {
                    MessageBox.Show(ex.Message, "Błąd", MessageBoxButtons.OK, MessageBoxIcon.Error);
                } else if (ex is ArgumentNullException || ex is FormatException || ex is OverflowException)
                {
                    MessageBox.Show("Tekst wprowadzony w pole tekstowe jest nie poprawny", "Błąd", MessageBoxButtons.OK, MessageBoxIcon.Error);
                } else
                {
                    MessageBox.Show("Coś poszło nie tak...", "Błąd", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }
    }
}
